
from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask_jsonpify import jsonify
import shutil



app = Flask(__name__)
api = Api(app)

class Info(Resource):
    def get(self):
        return {'version': 3, 'method': 'GET', 'message': 'Running'} # Fetches first column that is Employee ID

class DiskFree(Resource):
    def get(self):
        total, used, free = shutil.disk_usage("/")
        diskstatus = "Total %d used %d free %d" % ((total // (2**30)), (used // (2**30)), (free // (2**30)))
        return { 'version': 3, 'diskstatus': diskstatus }


api.add_resource(Info, '/get_info') # Route_1
api.add_resource(DiskFree, '/df')

if __name__ == '__main__':
     app.run(host='0.0.0.0', port='5290')
