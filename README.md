# netology_homework

## CI/CD

Вторая попытка. Первую пришлось удалить по некоторым причинам.

## Успешная публикация контейнера в ghcr.io/frstudent/netology_homework

Создался же.

## Тестирование

А вот тут засада - до merge образ не создаётся по условию задачи. При merge образ создаётся, но при этом issue закрывается автоматически. И куда же воткнуть тестера? 

<!--- 
## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/project/integrations/)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Automatically merge when pipeline succeeds](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:a25f78e8df513334f61b1837a32f58d2?https://docs.gitlab.com/ee/user/clusters/agent/)

***
-->

## Name
Одно из домашних заданий.


## License
Public domain.

## Project status
Вероятно что после окончания курсов проект будет удалён.
